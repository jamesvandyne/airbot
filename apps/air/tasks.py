from typing import Optional, Tuple
import io
import re
import logging
from time import sleep
import csv
from decimal import Decimal, InvalidOperation
from datetime import datetime

from bs4 import BeautifulSoup
from django.conf import settings
from django.utils.timezone import make_aware
import requests
import twitter

from .constants import MESSAGES, THRESHOLDS, MONITORING_LOCATIONS
from .models import Location, GCReading


logger = logging.getLogger(__name__)

include_map = {
    "include43202": "Ethane",
    "include43246": "2-Methyl-1-Pentene [P]",
    "include45209": "n-Propylbenzene [BP] *",
    "include43203": "Ethylene [P] *",
    "include43245": "1-Hexene [P]",
    "include45212": "m-Ethyltoluene [BP]",
    "include43204": "Propane [P] *",
    "include43231": "n-Hexane [BP] *",
    "include45213": "p-Ethyltoluene [BP]",
    "include43205": "Propylene [P] *",
    "include43289": "t-2-Hexene [BP]",
    "include45207": "1,3,5-Trimethylbenzene [BP] *",
    "include43214": "Isobutane [P] *",
    "include43290": "c-2-Hexene [BP]",
    "include45211": "o-ethyltoluene [BP]",
    "include43212": "n-Butane [P] *",
    "include43262": "Methylcyclopentane [BP] *",
    "include45208": "1,2,4-Trimethylbenzene [BP] *",
    "include43206": "Acetylene [P] *",
    "include43247": "2,4-Dimethylpentane [BP] *",
    "include43238": "n-Decane [BP] *",
    "include43216": "t-2-Butene [P] *",
    "include45201": "Benzene [BP] *",
    "include45225": "1,2,3-Trimethylbenzene [BP] *",
    "include43280": "1-Butene [P] *",
    "include43248": "Cyclohexane [BP] *",
    "include45218": "m-Diethylbenzene [BP]",
    "include43270": "Isobutene [P]",
    "include43263": "2-Methylhexane [BP] *",
    "include45219": "p-Diethylbenzene [BP]",
    "include43217": "c-2-Butene [P] *",
    "include43291": "2,3-Dimethylpentane [BP] *",
    "include43954": "n-Undecane [BP]",
    "include43242": "Cyclopentane [P] *",
    "include43249": "3-Methylhexane [BP] *",
    "include43256": "a-Pinene [BP]",
    "include43221": "Isopentane [P] *",
    "include43250": "2,2,4-Trimethylpentane [BP] *",
    "include43257": "b-Pinene [BP]",
    "include43220": "n-Pentane [P] *",
    "include43232": "n-Heptane [BP] *",
    "include43283": "Cyclopentene [P]",
    "include43218": "1,3-Butadiene [P] *",
    "include43261": "Methylcyclohexane [BP] *",
    "include43282": "3-Methyl-1-Butene [P]",
    "include43342": "3-Methyl-1-Butene+Cyclopentene [P]",
    "include43252": "2,3,4-Trimethylpentane [BP] *",
    "include61101": "Wind Speed",
    "include43226": "t-2-Pentene [P] *",
    "include45202": "Toluene [BP] *",
    "include61103": "Resultant Wind Speed",
    "include43228": "2-Methyl-2-Butene [P]",
    "include43960": "2-Methylheptane [BP] *",
    "include61104": "Resultant Wind Direction",
    "include43224": "1-Pentene [P] *",
    "include43253": "3-Methylheptane [BP] *",
    "include43227": "c-2-Pentene [P] *",
    "include43233": "n-Octane [BP] *",
    "include43244": "2,2-Dimethylbutane [P] *",
    "include45203": "Ethyl Benzene [BP] *",
    "include43284": "2,3-Dimethylbutane [P]",
    "include45109": "p-Xylene + m-Xylene [BP] *",
    "include43285": "2-Methylpentane [P]",
    "include45220": "Styrene [BP] *",
    "include43230": "3-Methylpentane [P]",
    "include45204": "o-Xylene [BP] *",
    "include43243": "Isoprene [P] *",
    "include43235": "n-Nonane [BP] *",
    "include43234": "4-Methyl-1-Pentene [P]",
    "include45210": "Isopropyl Benzene - Cumene [BP] *",
}
url = 'https://www.tceq.texas.gov/cgi-bin/compliance/monops/agc_daily_summary.pl'


def get_report(params, location) -> Optional[io.StringIO]:
    for key in include_map.keys():
        params[key] = 1
    response = requests.post(url, params)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'lxml')
        pre = soup.find('pre')
        data = io.StringIO()
        try:
            if pre.text[0] == '\n':
                data.write(pre.text[1:])
            else:
                data.write(pre.text[:])
            data.seek(0)
        except AttributeError:
            logger.warning("No data for %s %s/%s/%s" % (location,
                                                        params['user_month'] + 1,
                                                        params['user_day'],
                                                        params['user_year']))
            return
        return data


def get_pollutant(row) -> Optional[int]:
    try:
        pollutant = GCReading.lookup(row[0])
        return pollutant if pollutant != -1 else None
    except IndexError:
        return None


def extract_value(row, cell, site) -> Optional[Decimal]:
    try:
        return Decimal(cell)
    except InvalidOperation:
        # TODO: Make this into a simple dictionary lookup
        if cell == "NA":
            return None
        for non_value in GCReading.NON_VALUE_CHOICES:
            if cell == non_value[1]:
                return non_value[0]
    logger.error("Unknown value for %s %s", site, cell)


def save_reading(location: Location,
                 date: datetime,
                 pollutant,
                 value: Decimal) -> Tuple[GCReading, bool, Decimal]:
    created = True
    orig_value = value
    try:
        reading = GCReading.objects.get(location=location,
                                        date=date,
                                        pollutant=pollutant)
        created = False
        if reading.value != value:
            orig_value = reading.value
            reading.value = value
            reading.save()

    except GCReading.DoesNotExist:
        reading = GCReading.objects.create(location=location, date=date, pollutant=pollutant,
                                           value=value)
    except GCReading.MultipleObjectsReturned:
        GCReading.objects.filter(location=location,
                                 date=date,
                                 pollutant=pollutant).delete()
        reading = GCReading.objects.create(location=location,
                                           date=date,
                                           pollutant=pollutant,
                                           value=value)

    return reading, created, orig_value


def tweet_warning(site_id: str,
                  location_name: str,
                  pollutant: int,
                  created: bool,
                  value: Decimal,
                  orig_value: Decimal):
    """
    Tweet a health warning / updated value message if value or original value exceeds threshold
    """
    if not created and value == orig_value:
        # Value hasn't change, no need to tweet
        return

    if (site_id in MONITORING_LOCATIONS and
       pollutant in THRESHOLDS and
       (value > THRESHOLDS[pollutant] or orig_value > THRESHOLDS[pollutant])):

        tweet = MESSAGES[pollutant] % (location_name, value)
        if not created:
            tweet = "UPDATED VALUE: {}".format(tweet)
        twitter_api = twitter.Api(settings.TWITTER_APP_KEY,
                                  settings.TWITTER_APP_SECRET,
                                  settings.TWITTER_OAUTH_TOKEN,
                                  settings.TWITTER_OAUTH_TOKEN_SECRET)
        try:
            twitter_api.PostUpdate(tweet)
        except twitter.error.TwitterError as e:
            logger.warning(e.message)


def download_report(month=3,
                    day=28,
                    year=2016, site='48_201_0026', units='ppb-v', retry=0):
    """
    Expected format is a csv with variable length.

    The first column is the pollutant.
    Each column after that is the time from 0:00 -> 23:00 using 12 hour time

    :return:
    """
    location = Location.objects.get(site_id=site)
    params = {
        'select_date': 'user',
        'user_month': month - 1,  # convert to 0 index
        'user_day': day,
        'user_year': year,
        'user_site': site,
        'user_units': units,
        'report_format': 'comma',
    }
    report_data = get_report(params, location)
    if report_data:
        reader = csv.reader(report_data)
        header = None
        for row in reader:
            if header is None:
                header = row
                continue

            pollutant = get_pollutant(row)
            if not pollutant:
                continue

            for i, (title, cell) in enumerate(zip(header, row)):
                if i == 0:
                    continue

                date = datetime(month=month, day=day, year=year, hour=i - 1, minute=0, second=0)
                date = make_aware(date, timezone=GCReading.TZ)

                value = extract_value(row, cell, site)
                if not value:
                    continue
                reading, created, orig_value = save_reading(location, date, pollutant, value)
                tweet_warning(location.site_id, location.name, pollutant, created,
                              value, orig_value)

    elif retry < settings.RETRY_MAX:
        seconds_to_wait = 2.0 ** retry
        sleep(seconds_to_wait)
        # First countdown will be 1.0, then 2.0, 4.0, etc.
        download_report(month, year, day, site, units, retry + 1)
    else:
        logger.warning("Unable to download report site: %s %s date: %d/%d/%d",
                       location, site, year, month, day)


def download_locations(retry=0):
    """
    Scrape location site ids / names from the TCEQ website

    refs: https://bitbucket.org/jamesvandyne/airbot/issues/10/scrape-sites-daily
    """

    response = requests.get(url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'lxml')
        select = soup.find('select', {'name': 'user_site'})
        site_value = re.compile(r'\d{2}_\d{3}_\d{4}')

        for option in filter(lambda x: site_value.match(x['value']), select.find_all('option')):
            site_id = option['value']
            name = option.get_text(strip=True)
            try:
                location = Location.objects.get(site_id=site_id)
                if location.name != name:
                    logger.warning('Site name updated for %s %s', site_id, name)
                    location.name = name
                    location.save()
            except Location.DoesNotExist:
                location = Location.objects.create(site_id=site_id, name=name)
                logger.info("Created new location %s %s", (location.site_id, location.name))
    elif retry < settings.RETRY_MAX:
        seconds_to_wait = 2.0 ** retry
        sleep(seconds_to_wait)
        # First countdown will be 1.0, then 2.0, 4.0, etc.
        download_locations(retry + 1)
    else:
        logger.warning("Unable to update locations")
