import datetime
from decimal import Decimal

from django.db import models

from .constants import POLLUTANT_CHOICES

CST = datetime.timezone(datetime.timedelta(hours=-6))


class Location(models.Model):
    name = models.CharField(max_length=36)
    site_id = models.CharField(max_length=16)

    lat = models.DecimalField(max_digits=11, decimal_places=9, null=True)
    lon = models.DecimalField(max_digits=11, decimal_places=9, null=True)

    def __str__(self):
        return self.name


def pretty_value(value):
    if value < 0:
        idx = int(value.copy_abs() - 2)
        try:
            return GCReading.NON_VALUE_CHOICES[idx][1]
        except IndexError:
            pass
    return value


class GCReading(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    date = models.DateTimeField()
    value = models.DecimalField(max_digits=10,  decimal_places=2, null=True)
    pollutant = models.IntegerField(choices=POLLUTANT_CHOICES)
    TZ = CST
    BL = Decimal("-2")
    ST = Decimal("-3")
    _5PPB = Decimal("-4")
    AQI = Decimal("-5")
    ARC = Decimal("-6")
    AUDI = Decimal("-7")
    BLAN = Decimal("-8")
    CACS = Decimal("-9")
    CAL = Decimal("-10")
    DCSD = Decimal("-11")
    DLA = Decimal("-12")
    EXP = Decimal("-13")
    FEW = Decimal("-14")
    LIM = Decimal("-15")
    LST = Decimal("-16")
    MAL = Decimal("-17")
    MUL = Decimal("-18")
    NEG = Decimal("-19")
    NOD = Decimal("-20")
    NOL = Decimal("-21")
    PMA = Decimal("-22")
    QAS = Decimal("-23")
    QRE = Decimal("-24")
    RAS = Decimal("-25")
    RT_S = Decimal("-26")
    SPN = Decimal("-27")
    SPZ = Decimal("-28")
    UNK = Decimal("-29")
    # New / Undocumented Values
    IQ = Decimal("-30")
    CS = Decimal("-31")
    DL = Decimal("-32")
    XX = Decimal("-33")
    RA = Decimal("-34")

    NON_VALUE_CHOICES = (
        (BL, "BL"),
        (ST, "ST"),
        (_5PPB, "5PPB"),
        (AQI, "AQI"),
        (ARC, "ARC"),
        (AUDI, "AUDI"),
        (BLAN, "BLAN"),
        (CACS, "CACS"),
        (CAL, "CAL"),
        (DCSD, "DCSD"),
        (DLA, "DLA"),
        (EXP, "EXP"),
        (FEW, "FEW"),
        (LIM, "LIM"),
        (LST, "LST"),
        (MAL, "MAL"),
        (MUL, "MUL"),
        (NEG, "NEG"),
        (NOD, "NOD"),
        (NOL, "NOL"),
        (PMA, "PMA"),
        (QAS, "QAS"),
        (QRE, "QRE"),
        (RAS, "RAS"),
        (RT_S, "RT_S"),
        (SPN, "SPN"),
        (SPZ, "SPZ"),
        (UNK, "UNK"),
        (IQ, "IQ"),
        (CS, "CS"),
        (DL, "DL"),
        (XX, "XX"),
        (RA, "RA"),
    )

    class Meta:
        ordering = ("pollutant", "date")

    def __str__(self):
        return str(pretty_value(self.value))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._old_value = self.value

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Initial  _old_value is None.
        if self.value != self._old_value and self._old_value is not None:
            ReadingChangeLog.objects.create(reading=self,
                                            old_value=self._old_value,
                                            new_value=self.value)
        return super().save(force_insert=False,
                            force_update=False,
                            using=None,
                            update_fields=None)

    @classmethod
    def lookup(cls, name):
        for pollutant, display in POLLUTANT_CHOICES:
            if display == name:
                return pollutant
        return -1

    def pretty_value(self):
        return pretty_value(self.value)


class ReadingChangeLog(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    reading = models.ForeignKey(GCReading, on_delete=models.CASCADE)
    old_value = models.DecimalField(max_digits=10,  decimal_places=2, null=True)
    new_value = models.DecimalField(max_digits=10,  decimal_places=2, null=True)

    def __str__(self):
        return str(self.reading.pk)

    def pretty_old_value(self):
        return pretty_value(self.old_value)

    def pretty_new_value(self):
        return pretty_value(self.new_value)

    def location(self):
        return self.reading.location

    def pollutant(self):
        return self.reading.get_pollutant_display()
