from django.contrib import admin
from air.models import Location, GCReading, ReadingChangeLog
from air.constants import POLLUTANT_CHOICES
from django.contrib.admin import SimpleListFilter
from django.utils.timezone import localtime


class GCReadingPollutantFilter(SimpleListFilter):
    title = 'Pollutant'
    parameter_name = 'pollutant'

    def lookups(self, request, model_admin):
        return POLLUTANT_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(pollutant=self.value())
        return queryset


class GCReadingAdmin(admin.ModelAdmin):
    list_display = ('location', 'time', 'pollutant', 'pretty_value')
    list_filter = [GCReadingPollutantFilter, ('location', admin.RelatedOnlyFieldListFilter)]

    def time(self, instance):
        cst = localtime(instance.date, GCReading.TZ)
        return cst.strftime("%m/%d/%Y - %I %p")


class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'site_id')


class ReadingChangeLogAdmin(admin.ModelAdmin):
    list_display = ('reading__id', 'pollutant', 'location', 'pretty_old_value', 'pretty_new_value')

    def reading__id(self, instance):
        return instance.reading.pk


admin.site.register(Location, LocationAdmin)
admin.site.register(GCReading, GCReadingAdmin)
admin.site.register(ReadingChangeLog, ReadingChangeLogAdmin)
