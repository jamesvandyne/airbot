from air.models import Location, GCReading, ReadingChangeLog
from rest_framework import serializers


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ('url', 'name', 'site_id', 'lat', 'lon')


class ReadingChangeLogSerializer(serializers.HyperlinkedModelSerializer):
    location = serializers.CharField(source='location__name', read_only=True)
    old_value = serializers.CharField(source='pretty_old_value', read_only=True)
    new_value = serializers.CharField(source='pretty_new_value', read_only=True)

    class Meta:
        model = ReadingChangeLog
        fields = ('reading', 'created', 'location', 'pollutant', 'old_value', 'new_value')


class GCReadingSerializer(serializers.HyperlinkedModelSerializer):
    location = LocationSerializer()
    compound = serializers.CharField(source='get_pollutant_display')

    class Meta:
        model = GCReading
        fields = ('pollutant', 'compound', 'value', 'date', 'location', 'created', 'id')


class ReadingThresholdSerializer(serializers.HyperlinkedModelSerializer):
    avg = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    pollutant = serializers.IntegerField()
    reading_count = serializers.IntegerField(read_only=True)
    sum = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)

    class Meta:
        model = Location
        fields = ('pollutant', 'site_id', 'name', 'avg', 'pollutant', 'reading_count', 'sum')
