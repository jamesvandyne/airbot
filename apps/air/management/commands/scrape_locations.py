from django.core.management.base import BaseCommand
from air.tasks import download_locations
from air.models import Location


class Command(BaseCommand):
    help = 'Scrape site ids / names'

    def handle(self, *args, **options):

        self.stdout.write("Scraping site ids")
        before_count = Location.objects.count()
        download_locations()
        after_count = Location.objects.count()
        self.stdout.write("Location count before: %d after: %d" % (before_count, after_count))
