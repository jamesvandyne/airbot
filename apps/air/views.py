from rest_framework import viewsets
from django.db.models import Avg, F, Q, Sum, Count
from django.utils.timezone import now, timedelta, datetime
from air.models import Location, GCReading, ReadingChangeLog
from air.serializers import (LocationSerializer,
                             GCReadingSerializer,
                             ReadingThresholdSerializer,
                             ReadingChangeLogSerializer)


class LocationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows locations to be viewed.
    """
    queryset = Location.objects.all().order_by('name')
    serializer_class = LocationSerializer


class GCReadingViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows readings to be viewed.
    """
    queryset = GCReading.objects.all()
    serializer_class = GCReadingSerializer

    def get_queryset(self):
        query = self.queryset

        timestamp = self.request.query_params.get('date', None)
        if timestamp is not None:
            timestamp = datetime.fromtimestamp(int(timestamp))
        else:
            timestamp = now() - timedelta(days=1)

        try:
            pollutant = int(self.request.query_params.get('pollutant', None))
            pollutant_query = Q(pollutant=pollutant)
        except ValueError:
            pollutant = self.request.query_params.get('pollutant').split(",")
            pollutant_query = Q(pollutant__in=pollutant)
        except (TypeError, AttributeError):
            pollutant = F('pollutant')
            pollutant_query = Q(pollutant=pollutant)

        site_id = self.request.query_params.get('site_id', None)
        if site_id is not None:
            site_ids = site_id.split(",")
            if Location.objects.filter(site_id__in=site_ids).exists():
                pks = Location.objects.filter(site_id__in=site_ids).values_list('pk', flat=True)
                query = query.filter(location__pk__in=pks)
            else:
                query = query.none()

        # Only give us readings > 0 as error codes / statuses are always less than 0
        query = query.filter(Q(date__gte=timestamp) &
                             pollutant_query)

        return query


class ReadingChangeLogViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows us to see when values change
    """
    queryset = ReadingChangeLog.objects.all()
    serializer_class = ReadingChangeLogSerializer


class ReadingThresholdsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that averages the last 24 hours of each pollutant at each location
    """
    queryset = Location.objects.all()
    serializer_class = ReadingThresholdSerializer

    def get_queryset(self):
        query = self.queryset

        timestamp = self.request.query_params.get('date', None)
        if timestamp is not None:
            timestamp = datetime.fromtimestamp(int(timestamp))
        else:
            timestamp = now() - timedelta(days=1)

        try:
            pollutant = int(self.request.query_params.get('pollutant', None))
            pollutant_query = Q(gcreading__pollutant=pollutant)
        except ValueError:
            pollutant = self.request.query_params.get('pollutant').split(",")
            pollutant_query = Q(gcreading__pollutant__in=pollutant)
        except (TypeError, AttributeError):
            pollutant = F('gcreading__pollutant')
            pollutant_query = Q(gcreading__pollutant=pollutant)

        site_id = self.request.query_params.get('site_id', None)
        if site_id is not None:
            site_ids = site_id.split(",")
            if Location.objects.filter(site_id__in=site_ids).exists():
                query = query.filter(site_id__in=site_ids)
            else:
                query = query.none()

        # Only give us readings > 0 as error codes / statuses are always less than 0

        query = query.filter(Q(gcreading__date__gte=timestamp) &
                             Q(gcreading__value__gte='0.0') &
                             pollutant_query)
        # do our calculations
        query = query.annotate(avg=Avg('gcreading__value'),
                               pollutant=F('gcreading__pollutant'),
                               reading_count=Count('gcreading__pk'),
                               sum=Sum('gcreading__value'))
        query = query.values().order_by('id', 'pollutant')
        return query
