from decimal import Decimal


ETHANE = 2
ETHYLENE = 3
PROPANE = 4
PROPYLENE = 5
ISOBUTANE = 6
N_BUTANE = 7
ACETYLENE = 8
T_2_BUTENE = 9
_1_BUTENE = 10
C_2_BUTENE = 11
CYCLOPENTANE = 12
ISOPENTANE = 13
N_PENTANE = 14
_1_3_BUTADIENE = 15
T_2_PENTENE = 16
_1_PENTENE = 17
C_2_PENTENE = 18
_2_2_DIMETHYLBUTANE = 19
_2_METHYLPENTANE = 20
ISOPRENE = 21
N_HEXANE = 22
METHYLCYCLOPENTANE = 23
_2_4_DIMETHYLPENTANE = 24
BENZENE = 25
CYCLOHEXANE = 26
_2_METHYLHEXANE = 27
_2_3_DIMETHYLPENTANE = 28
_3_METHYLHEXANE = 29
_2_2_4_TRIMETHYLPENTANE = 30
N_HEPTANE = 31
METHYLCYCLOHEXANE = 32
_2_3_4_TRIMETHYLPENTANE = 33
TOLUENE = 34
_2_METHYLHEPTANE = 35
_3_METHYLHEPTANE = 36
N_OCTANE = 37
ETHYLBENZENE = 38
P_XYLENEM_XYLENE = 39
STYRENE = 40
O_XYLENE = 41
N_NONANE = 42
ISOPROPYLBENZENE_CUMENE = 43
N_PROPYLBENZENE = 44
_1_3_5_TRIMETHYLBENZENE = 45
_1_2_4_TRIMETHYLBENZENE = 46
N_DECANE = 47
_1_2_3_TRIMETHYLBENZENE = 48
_2_METHYL_2_BUTENE = 49
ETHANE_DISPLAY = "Ethane"
ETHYLENE_DISPLAY = "Ethylene"
PROPANE_DISPLAY = "Propane"
PROPYLENE_DISPLAY = "Propylene"
ISOBUTANE_DISPLAY = "Isobutane"
N_BUTANE_DISPLAY = "n-Butane"
ACETYLENE_DISPLAY = "Acetylene"
T_2_BUTENE_DISPLAY = "t-2-Butene"
_1_BUTENE_DISPLAY = "1-Butene"
C_2_BUTENE_DISPLAY = "c-2-Butene"
CYCLOPENTANE_DISPLAY = "Cyclopentane"
ISOPENTANE_DISPLAY = "Isopentane"
N_PENTANE_DISPLAY = "n-Pentane"
_1_3_BUTADIENE_DISPLAY = "1_3-Butadiene"
T_2_PENTENE_DISPLAY = "t-2-Pentene"
_1_PENTENE_DISPLAY = "1-Pentene"
C_2_PENTENE_DISPLAY = "c-2-Pentene"
_2_2_DIMETHYLBUTANE_DISPLAY = "2_2-Dimethylbutane"
_2_METHYLPENTANE_DISPLAY = "2-Methylpentane"
ISOPRENE_DISPLAY = "Isoprene"
N_HEXANE_DISPLAY = "n-Hexane"
METHYLCYCLOPENTANE_DISPLAY = "Methylcyclopentane"
_2_4_DIMETHYLPENTANE_DISPLAY = "2_4-Dimethylpentane"
BENZENE_DISPLAY = "Benzene"
CYCLOHEXANE_DISPLAY = "Cyclohexane"
_2_METHYLHEXANE_DISPLAY = "2-Methylhexane"
_2_3_DIMETHYLPENTANE_DISPLAY = "2_3-Dimethylpentane"
_3_METHYLHEXANE_DISPLAY = "3-Methylhexane"
_2_2_4_TRIMETHYLPENTANE_DISPLAY = "2_2_4-Trimethylpentane"
N_HEPTANE_DISPLAY = "n-Heptane"
METHYLCYCLOHEXANE_DISPLAY = "Methylcyclohexane"
_2_3_4_TRIMETHYLPENTANE_DISPLAY = "2_3_4-Trimethylpentane"
TOLUENE_DISPLAY = "Toluene"
_2_METHYLHEPTANE_DISPLAY = "2-Methylheptane"
_3_METHYLHEPTANE_DISPLAY = "3-Methylheptane"
N_OCTANE_DISPLAY = "n-Octane"
ETHYLBENZENE_DISPLAY = "Ethyl Benzene"
P_XYLENEM_XYLENE_DISPLAY = "p-Xylene + m-Xylene"
STYRENE_DISPLAY = "Styrene"
O_XYLENE_DISPLAY = "o-Xylene"
N_NONANE_DISPLAY = "n-Nonane"
ISOPROPYLBENZENE_CUMENE_DISPLAY = "Isopropyl Benzene - Cumene"
N_PROPYLBENZENE_DISPLAY = "n-Propylbenzene"
_1_3_5_TRIMETHYLBENZENE_DISPLAY = "1_3_5-Trimethylbenzene"
_1_2_4_TRIMETHYLBENZENE_DISPLAY = "1_2_4-Trimethylbenzene"
N_DECANE_DISPLAY = "n-Decane"
_1_2_3_TRIMETHYLBENZENE_DISPLAY = "1_2_3-Trimethylbenzene"
_2_METHYL_2_BUTENE_DISPLAY = "2-Methyl-2-Butene"
POLLUTANT_CHOICES = (
    (ETHANE, ETHANE_DISPLAY),
    (ETHYLENE, ETHYLENE_DISPLAY),
    (PROPANE, PROPANE_DISPLAY),
    (PROPYLENE, PROPYLENE_DISPLAY),
    (ISOBUTANE, ISOBUTANE_DISPLAY),
    (N_BUTANE, N_BUTANE_DISPLAY),
    (ACETYLENE, ACETYLENE_DISPLAY),
    (T_2_BUTENE, T_2_BUTENE_DISPLAY),
    (_1_BUTENE, _1_BUTENE_DISPLAY),
    (C_2_BUTENE, C_2_BUTENE_DISPLAY),
    (CYCLOPENTANE, CYCLOPENTANE_DISPLAY),
    (ISOPENTANE, ISOPENTANE_DISPLAY),
    (N_PENTANE, N_PENTANE_DISPLAY),
    (_1_3_BUTADIENE, _1_3_BUTADIENE_DISPLAY),
    (T_2_PENTENE, T_2_PENTENE_DISPLAY),
    (_1_PENTENE, _1_PENTENE_DISPLAY),
    (C_2_PENTENE, C_2_PENTENE_DISPLAY),
    (_2_2_DIMETHYLBUTANE, _2_2_DIMETHYLBUTANE_DISPLAY),
    (_2_METHYLPENTANE, _2_METHYLPENTANE_DISPLAY),
    (ISOPRENE, ISOPRENE_DISPLAY),
    (N_HEXANE, N_HEXANE_DISPLAY),
    (METHYLCYCLOPENTANE, METHYLCYCLOPENTANE_DISPLAY),
    (_2_4_DIMETHYLPENTANE, _2_4_DIMETHYLPENTANE_DISPLAY),
    (BENZENE, BENZENE_DISPLAY),
    (CYCLOHEXANE, CYCLOHEXANE_DISPLAY),
    (_2_METHYLHEXANE, _2_METHYLHEXANE_DISPLAY),
    (_2_3_DIMETHYLPENTANE, _2_3_DIMETHYLPENTANE_DISPLAY),
    (_3_METHYLHEXANE, _3_METHYLHEXANE_DISPLAY),
    (_2_2_4_TRIMETHYLPENTANE, _2_2_4_TRIMETHYLPENTANE_DISPLAY),
    (N_HEPTANE, N_HEPTANE_DISPLAY),
    (METHYLCYCLOHEXANE, METHYLCYCLOHEXANE_DISPLAY),
    (_2_3_4_TRIMETHYLPENTANE, _2_3_4_TRIMETHYLPENTANE_DISPLAY),
    (TOLUENE, TOLUENE_DISPLAY),
    (_2_METHYLHEPTANE, _2_METHYLHEPTANE_DISPLAY),
    (_3_METHYLHEPTANE, _3_METHYLHEPTANE_DISPLAY),
    (N_OCTANE, N_OCTANE_DISPLAY),
    (ETHYLBENZENE, ETHYLBENZENE_DISPLAY),
    (P_XYLENEM_XYLENE, P_XYLENEM_XYLENE_DISPLAY),
    (STYRENE, STYRENE_DISPLAY),
    (O_XYLENE, O_XYLENE_DISPLAY),
    (N_NONANE, N_NONANE_DISPLAY),
    (ISOPROPYLBENZENE_CUMENE, ISOPROPYLBENZENE_CUMENE_DISPLAY),
    (N_PROPYLBENZENE, N_PROPYLBENZENE_DISPLAY),
    (_1_3_5_TRIMETHYLBENZENE, _1_3_5_TRIMETHYLBENZENE_DISPLAY),
    (_1_2_4_TRIMETHYLBENZENE, _1_2_4_TRIMETHYLBENZENE_DISPLAY),
    (N_DECANE, N_DECANE_DISPLAY),
    (_1_2_3_TRIMETHYLBENZENE, _1_2_3_TRIMETHYLBENZENE_DISPLAY),
    (_2_METHYL_2_BUTENE, _2_METHYL_2_BUTENE_DISPLAY),
)


THRESHOLDS = {
    _1_3_BUTADIENE:  Decimal("1.99"),
    BENZENE: Decimal("2.97"),
    TOLUENE: Decimal("4.36"),
    ETHYLBENZENE: Decimal("0.67"),
    P_XYLENEM_XYLENE: Decimal("1.99"),
    STYRENE: Decimal("2.04"),
    O_XYLENE: Decimal("0.76"),
}

MESSAGES = {
    _1_3_BUTADIENE: "1,3 Butadiene levels are high at %s! %s ppb."
                    " This may cause respiratory issues.",
    BENZENE: "Benzene levels at %s are too high. %s ppb. This is a carcinogenic compound.",
    ETHYLBENZENE: "Ethylbenzene levels are high today at %s. %s ppb. May cause respiratory issues.",
    TOLUENE: "Toluene levels over threshold at %s. %s ppb. Toluene harms the nervous system.",
    STYRENE: "Styrene levels over threshold at %s. %s ppb. Chronic exposure harms nervous system.",
    O_XYLENE: "o-Xylene levels over threshold at %s. %s ppb. "
              "May cause throat and gastro irritation.",
    P_XYLENEM_XYLENE: "Xylene levels over threshold at %s. %s ppb."
                      " May cause throat and gastro irritation."
}

MONITORING_LOCATIONS = {
    "48_201_0057": "Galena Park",
    "48_039_1003": "Clute",
    "48_039_1004": "Manvel Croix Park",
    "48_201_0026": "Channelview",
    "48_039_1016": "Lake Jackson",
    "48_071_0013": "Smith Point Hawkins Camp",
    "48_071_1606": "UH Smith Point",
    "48_157_0696": "UH Sugarland",
    "48_167_0004": "Texas City Fire Station",
    "48_167_0005": "Texas City Ball Park",
    "48_167_0056": "Texas City 34th Street",
    "48_167_0571": "Clear Creek High School",
    "48_167_0615": "Texas City BP 31st Street Site 1",
    "48_167_0616": "Texas City BP Onsite (Site 2)",
    "48_167_0621": "Texas City BP Logan Street (Site 3)",
    "48_167_0683": "Texas City 11th Street",
    "48_167_0697": "UH Coastal Center",
    "48_167_1034": "Galveston 99th Street",
    "48_167_5005": "Galveston Airport KGLS",
    "48_201_0024": "Houston Aldine",
    "48_201_0029": "Northwest Harris County",
    "48_201_0036": "Jacinto Port",
    "48_201_0046": "Houston North Wayside",
    "48_201_0047": "Lang",
    "48_201_0051": "Houston Croquet",
    "48_201_0055": "Houston Bayland Park",
    "48_201_0058": "Baytown",
    "48_201_0060": "Houston Kirkpatrick",
    "48_201_0061": "Shore Acres",
    "48_201_0062": "Houston Monroe",
    "48_201_0066": "Houston Westhollow",
    "48_201_0069": "Milby Park",
    "48_201_0071": "Pasadena HL&P",
    "48_201_0075": "Houston Texas Avenue",
    "48_201_0307": "Manchester/Central",
    "48_201_0416": "Park Place",
    "48_201_0551": "Sheldon",
    "48_201_0552": "Baytown Wetlands Center",
    "48_201_0553": "Crosby Library",
    "48_201_0554": "West Houston",
    "48_201_0556": "La Porte Sylvan Beach",
    "48_201_0557": "Mercer Arboretum",
    "48_201_0558": "Tom Bass",
    "48_201_0559": "Katy Park",
    "48_201_0560": "Atascocita",
    "48_201_0561": "Meyer Park",
    "48_201_0562": "Bunker Hill Village",
    "48_201_0563": "Huffman Wolf Road",
    "48_201_0570": "Clear Brook High School",
    "48_201_0572": "Clear Lake High School",
    "48_201_0617": "Wallisville Road",
    "48_201_0669": "TPC FTIR South",
    "48_201_0670": "TPC FTIR North",
    "48_201_0671": "Goodyear GC",
    "48_201_0673": "Goodyear Houston Site 2",
    "48_201_0695": "UH Moody Tower",
    "48_201_0803": "HRM 3 Haden Rd",
    "48_201_1015": "Lynchburg Ferry",
    "48_201_1017": "Baytown Garth",
    "48_201_1034": "Houston East",
    "48_201_1035": "Clinton",
    "48_201_1039": "Houston Deer Park 2",
    "48_201_1042": "Kingwood",
    "48_201_1043": "La Porte Airport C243",
    "48_201_1049": "Pasadena North",
    "48_201_1050": "Seabrook Friendship Park",
    "48_201_1052": "Houston North Loop",
    "48_201_1066": "Houston Southwest Freeway",
    "48_201_6000": "Cesar Chavez",
    "48_291_0699": "UH West Liberty",
    "48_339_0078": "Conroe Relocated",
    "48_339_0698": "UH WG Jones Forest",
    "48_339_5006": "Conroe Airport KCXO",
    "48_471_5012": "Huntsville KUTS"
}
