import json
import pytest
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from freezegun import freeze_time


@freeze_time('2019-05-24 08:00:00')
@pytest.mark.django_db
class TestReadingsAPI:

    @pytest.fixture
    def target(self):
        from air.views import GCReadingViewSet
        return GCReadingViewSet.as_view()

    @pytest.fixture
    def api_path(self):
        return '/readings/'

    @pytest.fixture
    def expected_format(self, gcreading):
        return {
          "count": 1,
          "next": None,
          "previous": None,
          "results": [
            {
              "pollutant": 2,
              "compound": "Ethane",
              "value": "2.67",
              "date": gcreading.date.isoformat().replace('+00:00', 'Z'),
              "location": {
                "url": "http://testserver/locations/1/",
                "name": "Fort Worth Joe B Rushing Road [65]",
                "site_id": "48_439_1065",
                "lat": None,
                "lon": None
              },
              "created": gcreading.created.isoformat().replace('+00:00', 'Z'),
              "id": 1
            },
          ]
        }

    @pytest.fixture
    def gcreading(self, location):
        return mommy.make('GCReading',
                          id=1,
                          location=location,
                          value=2.67,
                          pollutant=2)

    def test_path(self, api_path):
        assert api_path == reverse('gcreading-list')

    def test_gets_readings(self, api_path, api_client, gcreading, expected_format):
        res = api_client.get(api_path)
        assert res.status_code == status.HTTP_200_OK
        assert json.loads(res.content) == expected_format
