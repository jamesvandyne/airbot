from unittest import mock
from decimal import Decimal
import datetime
from django.utils.timezone import make_aware
import pytest
from model_mommy import mommy

from air.models import Location


@pytest.mark.django_db
class TestDownloadLocation:

    @pytest.fixture
    def target(self):
        from air.tasks import download_locations
        return download_locations

    @pytest.fixture
    def location(self):
        return mommy.make('Location',
                          id=1,
                          name='Channelview [R]',
                          site_id='48_201_0026',
                          lat=None,
                          lon=None)

    @pytest.fixture
    def change_name_location(self):
        return mommy.make('Location',
                          id=1,
                          name='Channelview',
                          site_id='48_201_0026',
                          lat=None,
                          lon=None)

    @pytest.fixture
    def get_report(self, monkeypatch, location_response):
        res = mock.Mock()
        res.text = location_response
        res.status_code = 200

        get = mock.Mock(return_value=res)
        monkeypatch.setattr('air.tasks.requests.get', get)
        return get

    def test_creates_locations(self, target, location, get_report):
        assert Location.objects.count() == 1
        target()
        assert Location.objects.count() == 36
        assert Location.objects.all().values_list('site_id', 'name')[1] == ('48_135_0003',
                                                                            'Odessa Hays [N]')

    def test_updates_location_name(self, target, change_name_location, get_report):
        assert change_name_location.name == 'Channelview'
        target()
        change_name_location.refresh_from_db()
        assert change_name_location.name == 'Channelview [R]'
