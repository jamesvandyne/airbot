from unittest import mock
from io import StringIO

import pytest


@pytest.mark.django_db
class TestGetReport:

    @pytest.fixture
    def target(self):
        from air.tasks import get_report
        return get_report

    @pytest.fixture
    def valid_response(self, monkeypatch, page_response):
        res = mock.Mock()
        res.text = page_response
        res.status_code = 200

        post = mock.Mock(return_value=res)
        monkeypatch.setattr('air.tasks.requests.post', post)
        return res

    @pytest.fixture
    def invalid_response(self, monkeypatch, page_response):
        res = mock.Mock()
        res.text = None
        res.status_code = 400

        post = mock.Mock(return_value=res)
        monkeypatch.setattr('air.tasks.requests.post', post)
        return res

    def test_returns_data(self, location, valid_response, target):
        res = target({}, location)
        assert isinstance(res, StringIO)

    def test_non_200_returns_none(self, location, invalid_response, target):
        res = target({}, location)
        assert res is None
