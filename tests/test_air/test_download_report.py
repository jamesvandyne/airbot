from unittest import mock
from decimal import Decimal
import datetime

from air.models import GCReading
from django.utils.timezone import make_aware
import pytest
from model_mommy import mommy


@pytest.mark.django_db
class TestDownloadReport:

    @pytest.fixture
    def target(self):
        from air.tasks import download_report
        return download_report

    @pytest.fixture
    def twitter_post(self, monkeypatch, page_response):
        twitter_post = mock.Mock()
        twitter_api = mock.Mock(return_value=twitter_post)
        monkeypatch.setattr('air.tasks.twitter.Api', twitter_api)
        return twitter_post

    @pytest.fixture
    def location(self):
        return mommy.make('Location',
                          id=1,
                          name='Channelview [R]',
                          site_id='48_201_0026',
                          lat=None,
                          lon=None)

    @pytest.fixture
    def get_report(self, monkeypatch, single_page_response):
        res = mock.Mock()
        res.text = single_page_response
        res.status_code = 200

        post = mock.Mock(return_value=res)
        monkeypatch.setattr('air.tasks.requests.post', post)
        return post

    @pytest.fixture
    def get_exceeding_report(self, monkeypatch, exceeding_page_response):
        res = mock.Mock()
        res.text = exceeding_page_response
        res.status_code = 200

        post = mock.Mock(return_value=res)
        monkeypatch.setattr('air.tasks.requests.post', post)
        return post

    def test_saves_gcreadings(self, target, location, twitter_post, get_report):
        assert GCReading.objects.count() == 0
        target(month=6, day=6, year=2019, site=location.site_id)
        assert GCReading.objects.count() == 15
        twitter_post.PostUpdate.assert_not_called()

    def test_updates_existing_readings(self, target, location, twitter_post, get_report):
        assert GCReading.objects.count() == 0
        target(month=6, day=6, year=2019, site=location.site_id)
        assert GCReading.objects.count() == 15

        target(month=6, day=6, year=2019, site=location.site_id)
        assert GCReading.objects.count() == 15

        twitter_post.PostUpdate.assert_not_called()

    def test_exceeding_threshold_tweets(self, target, location, twitter_post, get_exceeding_report):
        target(month=6, day=6, year=2019, site=location.site_id)
        expected = "Benzene levels at {} are too high. {} ppb. " \
                   "This is a carcinogenic compound.".format(location.name, "1.5")
        calls = [mock.call(expected)]
        assert twitter_post.PostUpdate.mock_calls == calls

    @pytest.fixture
    def safe_reading(self, location):
        date = datetime.datetime(2019, 6, 6, 2, 0)
        return mommy.make("GCReading",
                          location=location,
                          pollutant=25,
                          date=make_aware(date, timezone=GCReading.TZ),
                          value=Decimal("1.0")
                          )

    def test_updates_threshold_tweets(self, target, location, twitter_post, get_exceeding_report,
                                      safe_reading):
        target(month=6, day=6, year=2019, site=location.site_id)
        expected = "UPDATED VALUE: Benzene levels at {} are too high. {} ppb. " \
                   "This is a carcinogenic compound.".format(location.name, "1.5")
        calls = [mock.call(expected)]
        assert twitter_post.PostUpdate.mock_calls == calls
