from unittest import mock
from decimal import Decimal

import pytest


@pytest.mark.django_db
class TestTweetWarning:

    @pytest.fixture
    def target(self):
        from air.tasks import tweet_warning
        return tweet_warning

    @pytest.fixture
    def twitter_post(self, monkeypatch, page_response):
        twitter_post = mock.Mock()
        twitter_api = mock.Mock(return_value=twitter_post)
        monkeypatch.setattr('air.tasks.twitter.Api', twitter_api)
        return twitter_post

    @pytest.fixture
    def monitored_location(self):
        return "48_201_0026"

    @pytest.fixture
    def monitored_name(self):
        return "Channelview"

    @pytest.fixture
    def monitored_pollutant(self):
        from air.constants import BENZENE
        return BENZENE

    @pytest.fixture
    def nonmonitored_pollutant(self):
        from air.constants import N_DECANE
        return N_DECANE

    @pytest.fixture
    def threshold(self, monitored_pollutant):
        from air.constants import THRESHOLDS
        return THRESHOLDS[monitored_pollutant]

    @pytest.fixture
    def warning_message(self, monitored_pollutant):
        from air.constants import MESSAGES
        return MESSAGES[monitored_pollutant]

    def test_tweets_warning_on_create(self, target, monitored_location, monitored_pollutant,
                                      monitored_name, threshold, twitter_post, warning_message):
        exceeding_threshold = threshold + Decimal("1.0")
        expected = warning_message % (monitored_name, exceeding_threshold)
        target(monitored_location, monitored_name, monitored_pollutant, True, exceeding_threshold,
               exceeding_threshold)
        calls = [mock.call(expected)]
        assert twitter_post.PostUpdate.mock_calls == calls

    def test_tweets_when_updating(self, target, monitored_location, monitored_pollutant,
                                      monitored_name, threshold, twitter_post, warning_message):
        exceeding_threshold = threshold + Decimal("1.0")
        base_message = warning_message % (monitored_name, exceeding_threshold)
        expected = "UPDATED VALUE: {}".format(base_message)
        target(monitored_location, monitored_name, monitored_pollutant, False, exceeding_threshold,
               threshold)
        calls = [mock.call(expected)]
        assert twitter_post.PostUpdate.mock_calls == calls

    def test_tweets_only_updates_exceeding(self, target, monitored_location, monitored_pollutant,
                                      monitored_name, threshold, twitter_post, warning_message):
        target(monitored_location, monitored_name, monitored_pollutant, True, threshold,
               threshold)
        twitter_post.PostUpdate.assert_not_called()

    def test_skips_non_monitored_location(self, target, monitored_pollutant, threshold, twitter_post,
                                          warning_message):
        exceeding_threshold = threshold + Decimal("1.0")
        target("48_999_0026", "Not Channelview", monitored_pollutant, True, exceeding_threshold,
               exceeding_threshold)
        twitter_post.PostUpdate.assert_not_called()
