import pytest
from model_mommy import mommy
from rest_framework.test import APIClient


@pytest.fixture
def user():
    return mommy.make('User')


@pytest.fixture
def auth_token(user):
    return mommy.make('Token',
                      user=user)


@pytest.fixture
def api_client(user, auth_token):
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Token ' + auth_token.key)
    return client


@pytest.fixture
def location():
    return mommy.make('Location',
                      id=1,
                      name='Fort Worth Joe B Rushing Road [65]',
                      site_id='48_439_1065',
                      lat=None,
                      lon=None)


@pytest.fixture
def page_response():
    with open('tests/resources/channelview_response.html') as f:
        return f.read()


@pytest.fixture
def single_page_response():
    with open('tests/resources/single_channelview_response.html') as f:
        return f.read()


@pytest.fixture
def exceeding_page_response():
    with open('tests/resources/single_exceeding_channelview_response.html') as f:
        return f.read()


@pytest.fixture
def location_response():
    with open('tests/resources/get_location.html') as f:
        return f.read()
